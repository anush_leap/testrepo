package com.leap.android_ui_kit.extensions

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import java.util.*

fun View.visibilitygone() {
    this.visibility = View.GONE
}

fun View.visibilityVisible() {
    this.visibility = View.VISIBLE
}

fun View.visibilityInvisible() {
    this.visibility = View.INVISIBLE
}

fun TextView.handleUrlClicks(onClicked: ((String) -> Unit)? = null) {
    //create span builder and replaces current text with it
    text = SpannableStringBuilder.valueOf(text).apply {
        //search for all URL spans and replace all spans with our own clickable spans
        getSpans(0, length, URLSpan::class.java).forEach {
            //add new clickable span at the same position
            setSpan(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        onClicked?.invoke(it.url)
                    }
                },
                getSpanStart(it),
                getSpanEnd(it),
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE
            )
            //remove old URLSpan
            removeSpan(it)
        }
    }
    //make sure movement method is set
    movementMethod = LinkMovementMethod.getInstance()
}

fun EditText.showKeyboard() {
    var parent = parent
    while (parent != null && parent is ViewGroup) {
        if (parent.descendantFocusability == ViewGroup.FOCUS_BLOCK_DESCENDANTS) {
            parent.descendantFocusability = ViewGroup.FOCUS_AFTER_DESCENDANTS
            break
        }
        parent = parent.parent
    }

    this.clearFocus()
    this.requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

fun toCamelCase(s: String?): String? {
    var s = s
    if (s == null) {
        return null
    }
    s = s.toUpperCase(Locale.ENGLISH)
    val ns = s.length
    val sa = s.toCharArray()
    if (ns > 0) sa[0] = Character.toUpperCase(sa[0])
    for (i in 1 until ns) {
        // 39 is apostrophe
        if (!Character.isLetter(sa[i - 1]) && sa[i - 1].toInt() != 39) sa[i] = Character.toUpperCase(sa[i]) else sa[i] = Character.toLowerCase(sa[i])
    }
    s = String(sa)
    return s
}