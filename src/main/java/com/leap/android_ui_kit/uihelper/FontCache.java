package com.leap.android_ui_kit.uihelper;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Helper class which loads and returns typeface
 **/
public class FontCache {
    private static Typeface montserrat_regular, worksans_regular;
    private static Typeface paulgrotesk_bold, paulgrotesk_regular;

    public static Typeface getPaulGroteskBoldFont(Context context) {
        if (paulgrotesk_bold == null) {
            paulgrotesk_bold = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/paulgrotesk_bold_trail.otf");
        }

        return paulgrotesk_bold;
    }

    public static Typeface getIskoWebRegularFont(Context context) {
        if (paulgrotesk_regular == null) {
            paulgrotesk_regular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/paulgrotesk_regular_trail.otf");
        }
        return paulgrotesk_regular;
    }

    public static Typeface getWorksansRegularFont(Context context) {
        if (worksans_regular == null) {
            worksans_regular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/paulgrotesk_regular_trail.otf");
        }
        return worksans_regular;
    }

    public static Typeface getWorksansRegularFontUpdated(Context context) {
        if (worksans_regular == null) {
            worksans_regular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/worksans_regular.ttf");
        }
        return worksans_regular;
    }

    public static Typeface getMontserratRegularFont(Context context) {
        if (montserrat_regular == null) {
            montserrat_regular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/paulgrotesk_regular_trail.otf");
        }
        return montserrat_regular;
    }

    public static Typeface getMontserratRegularFontUpdated(Context context) {
        if (montserrat_regular == null) {
            montserrat_regular = Typeface.createFromAsset(context.getResources()
                    .getAssets(), "fonts/montserrat_regular.otf");
        }
        return montserrat_regular;
    }


}