package com.leap.android_ui_kit.widget

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.leap.android_ui_kit.R

/**
 * Created by ashish on 13/07/15.
 * Use ONLY this class to create a new DialogBox object.
 */
open class CustomDialogBox(private val mContext: Context) : Dialog(mContext, R.style.DialogColorTheme) {
    override fun setContentView(layoutId: Int) {
        super.setContentView(layoutId)
        setLayoutParams()
    }

    fun setContentView(view: View?, matchParentWidth: Boolean) {
        setContentView(view!!)
        if (matchParentWidth) {
            setLayoutParams()
        }
    }

    /*
          TODO: If width of Dialogbox not set to match_parent for low resolution devices then app
           crashes while setting '...'
                in the chooseQuantity for dishes popup in DishesBaseAdapter class.
          For low resolution devices, use default padding
          For high resolution devices give 24dp padding from left and right side of the dialog box.
          Default padding of dialog boxes is 16dp so give 8 dp from each side.
        */
    private fun setLayoutParams() {
        if (mContext.resources.configuration.smallestScreenWidthDp > 320) {
            window!!.setLayout((mContext.resources.displayMetrics.widthPixels
                    - mContext.resources.getDimension(R.dimen.d_user_permissible_padding)).toInt(),
                    WindowManager.LayoutParams.WRAP_CONTENT)
        } else {
            window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
    }

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setCancelable(true)
        setCanceledOnTouchOutside(true)
    }
}