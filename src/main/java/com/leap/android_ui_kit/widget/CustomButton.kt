package com.leap.android_ui_kit.widget

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.leap.android_ui_kit.R
import com.leap.android_ui_kit.extensions.toCamelCase
import com.leap.android_ui_kit.uihelper.FontCache


class CustomButton : AppCompatButton {
    private var mTextCase = 0

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setTextStyle(context, attrs)
        setTextCase(context, attrs)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            stateListAnimator = null
        }
        /*if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            setCompoundDrawables(null, null, null, null)
        }*/
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setTextStyle(context, attrs)
        setTextCase(context, attrs)
    }

    private fun setTextStyle(context: Context, attrs: AttributeSet?) {
        val mTextStyle: Int
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomButton, 0, 0)
        mTextStyle = try {
            a
                    .getInteger(R.styleable.CustomButton_text_weight, 1)
        } finally {
            a.recycle()
        }
        when (mTextStyle) {
            0 -> typeface = normalFont
            1 -> typeface = boldFont
            else -> {
            }
        }
    }

    private fun setTextCase(context: Context, attrs: AttributeSet?) {
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomButton, 0, 0)
        mTextCase = try {
            a.getInteger(R.styleable.CustomButton_text_case, 0)
        } finally {
            a.recycle()
        }
        setText(text, BufferType.SPANNABLE)
    }

    override fun setText(ch: CharSequence, type: BufferType) {
        when (mTextCase) {
            0 -> super.setText(ch, type)
            1 -> {
                var s: String? = ch.toString()
                s = toCamelCase(s)
                super.setText(s, type)
            }
            2 -> {
                var s = ch.toString()
                s = s.toLowerCase()
                super.setText(s, type)
            }
            3 -> {
                var s = ch.toString()
                s = s.toUpperCase()
                super.setText(s, type)
            }
            else -> {
            }
        }
    }

    private val boldFont: Typeface
        private get() = FontCache.getPaulGroteskBoldFont(context)
    private val normalFont: Typeface
        private get() = FontCache.getIskoWebRegularFont(context)

    companion object {
        private const val DEBUG_TAG = "CustomButton"
    }
}