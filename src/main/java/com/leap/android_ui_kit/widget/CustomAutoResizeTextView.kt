package com.leap.android_ui_kit.widget

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Handler
import android.text.Spanned
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import com.leap.android_ui_kit.R
import com.leap.android_ui_kit.extensions.toCamelCase
import com.leap.android_ui_kit.uihelper.FontCache

class CustomAutoResizeTextView : AppCompatTextView {
    protected var mTextCase = 0
    var mResources: Resources? = null
    private val measureHandler = Handler()
    private val requestLayout = Runnable { requestLayout() }

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (!isInEditMode) {
            setTextStyle(context, attrs)
            setTextCase(context, attrs)
        }
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setTextStyle(context, attrs)
        setTextCase(context, attrs)
    }

    override fun onMeasure(widthMeasureSpec: Int,
                           heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val maxWidth = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val maxHeight = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        if (maxWidth < 1.0f || maxHeight < 1.0f) {
            return
        }
        var index = 0
        var lineCount = 0
        val text = text
        val paint = paint
        while (index < text.length) {
            index += paint.breakText(text, index, text.length, true, maxWidth, null)
            lineCount++
        }

        val height: Float = (lineCount * lineHeight).plus(if (lineCount > 0) ((lineCount - 1) * paint.fontSpacing).toInt() else 0).toFloat()

        if (height > maxHeight) {
            val textSize = textSize
            setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize - 1)
            measureHandler.post(requestLayout)
        }
    }

    private fun setTextStyle(context: Context, attrs: AttributeSet?) {
        mResources = resources
        val mTextStyle: Int
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0)
        mTextStyle = try {
            a
                    .getInteger(R.styleable.CustomTextView_text_weight, 0)
        } finally {
            a.recycle()
        }
        when (mTextStyle) {
            0 -> typeface = boldFont
            1 -> typeface = normalFont
            else -> {
            }
        }
    }

    protected fun setTextCase(context: Context, attrs: AttributeSet?) {
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0)
        mTextCase = try {
            a.getInteger(R.styleable.CustomTextView_text_case, 0)
        } finally {
            a.recycle()
        }
        val text = text
        setText(text, if (text is Spanned) BufferType.SPANNABLE else BufferType.NORMAL)
    }

    override fun setText(ch: CharSequence, type: BufferType) {
        when (mTextCase) {
            0 -> super.setText(ch, type)
            1 -> {
                var s: String? = ch.toString()
                s = toCamelCase(s)
                super.setText(s, type)
            }
            2 -> {
                var s = ch.toString()
                s = s.toLowerCase()
                super.setText(s, type)
            }
            3 -> {
                var s = ch.toString()
                s = s.toUpperCase()
                super.setText(s, type)
            }
            else -> {
            }
        }
    }

    private val boldFont: Typeface
        private get() = FontCache.getPaulGroteskBoldFont(context)
    private val normalFont: Typeface
        private get() = FontCache.getIskoWebRegularFont(context)
}