package com.leap.android_ui_kit.widget

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.text.Spanned
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.leap.android_ui_kit.R
import com.leap.android_ui_kit.uihelper.FontCache
import java.util.*

open class CustomTextView : AppCompatTextView {
    protected var mTextCase = 0
    var mResources: Resources? = null
    private val mContext: Context? = null

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (!isInEditMode) {
            setTextStyle(context, attrs)
            setTextCase(context, attrs)
        }
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setTextStyle(context, attrs)
        setTextCase(context, attrs)
    }

    private fun setTextStyle(context: Context, attrs: AttributeSet?) {
        mResources = resources
        val mTextStyle: Int
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0)
        mTextStyle = try {
            a
                    .getInteger(R.styleable.CustomTextView_text_weight, 0)
        } finally {
            a.recycle()
        }
        when (mTextStyle) {
            0 -> typeface = normalFont
            1 -> typeface = boldFont
            2 -> typeface = montserratRegularFont
            3 -> typeface = worksansRegularFont
            else -> {
            }
        }
    }

    protected fun setTextCase(context: Context, attrs: AttributeSet?) {
        val a = context.theme.obtainStyledAttributes(attrs,
                R.styleable.CustomTextView, 0, 0)
        mTextCase = try {
            a.getInteger(R.styleable.CustomTextView_text_case, 0)
        } finally {
            a.recycle()
        }
        val text = text
        setText(text, if (text is Spanned) BufferType.SPANNABLE else BufferType.NORMAL)
    }

    override fun setText(ch: CharSequence, type: BufferType) {
        when (mTextCase) {
            0 -> super.setText(ch, type)
            1 -> {
                var s: String? = ch.toString()
                s = toCamelCase(s)
                super.setText(s, type)
            }
            2 -> {
                var s = ch.toString()
                s = s.toLowerCase()
                super.setText(s, type)
            }
            3 -> {
                var s = ch.toString()
                s = s.toUpperCase()
                super.setText(s, type)
            }
            else -> {
            }
        }
    }

    private val boldFont: Typeface
        private get() = FontCache.getPaulGroteskBoldFont(context)
    private val normalFont: Typeface
        private get() = FontCache.getIskoWebRegularFont(context)
    private val worksansRegularFont: Typeface
        private get() = FontCache.getWorksansRegularFont(context)
    private val montserratRegularFont: Typeface
        private get() = FontCache.getMontserratRegularFont(context)

    fun toCamelCase(s: String?): String? {
        var s = s
        if (s == null) {
            return null
        }
        s = s.toUpperCase(Locale.ENGLISH)
        val ns = s.length
        val sa = s.toCharArray()
        if (ns > 0) sa[0] = Character.toUpperCase(sa[0])
        for (i in 1 until ns) {
            // 39 is apostrophe
            if (!Character.isLetter(sa[i - 1]) && sa[i - 1].toInt() != 39) sa[i] = Character.toUpperCase(sa[i]) else sa[i] = Character.toLowerCase(sa[i])
        }
        s = String(sa)
        return s
    }
}