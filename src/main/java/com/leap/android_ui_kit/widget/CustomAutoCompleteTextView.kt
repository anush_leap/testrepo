package com.leap.android_ui_kit.widget

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatAutoCompleteTextView


class CustomAutoCompleteTextView(context: Context, attrs: AttributeSet?) : AppCompatAutoCompleteTextView(context, attrs) {

    override fun enoughToFilter(): Boolean {
        return true
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            performFiltering(text, 0)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        showDropDown()
        return super.onTouchEvent(event)
    }
}
