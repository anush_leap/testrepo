package com.leap.android_ui_kit.widget

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.leap.android_ui_kit.R
import com.leap.android_ui_kit.extensions.toCamelCase
import com.leap.android_ui_kit.uihelper.FontCache

class CustomEditBox : AppCompatEditText {
    protected var mTextCase = 0
    var mResources: Resources? = null

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        if (!isInEditMode) {
            setTextStyle(context, attrs)
            setTextCase(context, attrs)
        }
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        setTextStyle(context, attrs)
        setTextCase(context, attrs)
    }

    private fun setTextStyle(context: Context, attrs: AttributeSet?) {
        mResources = resources
        val mTextStyle: Int
        val a = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomTextView, 0, 0
        )
        mTextStyle = try {
            a
                .getInteger(R.styleable.CustomTextView_text_weight, 0)
        } finally {
            a.recycle()
        }
        when (mTextStyle) {
            0 -> typeface = normalFont
            1 -> typeface = boldFont
            else -> {
            }
        }
    }

    protected fun setTextCase(context: Context, attrs: AttributeSet?) {
        val a = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CustomTextView, 0, 0
        )
        mTextCase = try {
            a.getInteger(R.styleable.CustomTextView_text_case, 0)
        } finally {
            a.recycle()
        }
        val text: CharSequence? = text
        setText(text!!, if (text != null) BufferType.SPANNABLE else BufferType.NORMAL)
    }

    override fun setText(ch: CharSequence, type: BufferType) {
        when (mTextCase) {
            0 -> super.setText(ch, type)
            1 -> {
                var s: String? = ch.toString()
                s = toCamelCase(s)
                super.setText(s, type)
            }
            2 -> {
                var s = ch.toString()
                s = s.toLowerCase()
                super.setText(s, type)
            }
            3 -> {
                var s = ch.toString()
                s = s.toUpperCase()
                super.setText(s, type)
            }
            else -> {
            }
        }
    }

    private val boldFont: Typeface
        private get() = FontCache.getPaulGroteskBoldFont(context)
    private val normalFont: Typeface
        private get() = FontCache.getIskoWebRegularFont(context)
}