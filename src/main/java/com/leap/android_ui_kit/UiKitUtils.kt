package com.leap.android_ui_kit

import android.app.Application

class UiKitUtils {

    companion object {
        lateinit var application: Application

        fun setUpUIKit(application: Application) {
            Companion.application = application
        }
    }
}