package com.leap.android_ui_kit.imagehelper

import android.app.Activity
import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.provider.MediaStore
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

import com.leap.android_ui_kit.R
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.ByteArrayOutputStream
import java.util.*


object ImageUtils {
    const val TAG = "ImageUtils"

    fun loadRoundedCornerImage(imageView: ImageView, context: Context?, url: String?) {
        url?.let {
            imageView.post {
                if (isValidContext(context)) {
                    try {
                        context?.let { it1 ->
                            Glide.with(it1) //1
                                .load(url)
                                .placeholder(R.drawable.ic_leap_logo_placeholder)
                                .fallback(R.drawable.ic_leap_logo_placeholder)
                                .error(R.drawable.ic_leap_logo_placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE) // // 3
                                .apply(RequestOptions.bitmapTransform(RoundedCorners(10)))
                                .into(imageView)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

        }
    }

    fun loadImageWithoutRoundedCorner(imageView: ImageView, context: Context?, url: String?) {
        url?.let {
            imageView.post {
                if (isValidContext(context)) {
                    try {
                        context?.let { it1 ->
                            Glide.with(it1) //1
                                .load(url)
                                .placeholder(R.drawable.ic_leap_logo_placeholder)
                                .fallback(R.drawable.ic_leap_logo_placeholder)
                                .error(R.drawable.ic_leap_logo_placeholder)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE) // // 3
                                .into(imageView)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

        }
    }

    fun loadRoundImage(imageView: ImageView, context: Context?, url: String?) {
        url?.let {
            imageView.post {
                if (isValidContext(context)) {
                    try {
                        context?.let { it1 ->
                            Glide.with(it1) //1
                                .load(url)
                                .placeholder(R.drawable.img_stories_default_bg)
                                .fallback(R.drawable.img_stories_default_bg)
                                .error(R.drawable.img_stories_default_bg)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE) // // 3
                                .circleCrop()
                                .into(imageView)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

        }
    }



    fun loadRoundImageOnUI(imageView: ImageView, context: Context?, url: String?) {
        url?.let {
                if (isValidContext(context)) {
                    try {
                        context?.let { it1 ->
                            Glide.with(it1) //1
                                .load(url)
                                .placeholder(R.drawable.img_stories_default_bg)
                                .fallback(R.drawable.img_stories_default_bg)
                                .error(R.drawable.img_stories_default_bg)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE) // // 3
                                .circleCrop()
                                .dontAnimate()
                                .into(imageView)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
        }
    }

    private fun isValidContext(context: Context?): Boolean {
        if (context == null) {
            return false
        } else if (context !is Application) {
            if (context is FragmentActivity) {
                if (context.isDestroyed) {
                    return false
                }
            } else if (context is Activity) {
                if (context.isDestroyed) {
                    return false
                }
            }
        }
        return true
    }

    fun getBitmapFromView(view: ImageView): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    fun getImageUri(
        context: Context,
        bitmap: Bitmap?,
    ): Uri? {
        bitmap?.let {
            val bytes = ByteArrayOutputStream()
            it.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                context.contentResolver,
                it,
                "IMG__${Calendar.getInstance().time}",
                null
            )

            if (path != null)
                return Uri.parse(path)
        }
        return null
    }

    fun createBitmapFromUrl(url: String): Bitmap? {
        val req = Request.Builder().url(url).build()
        val res = OkHttpClient().newCall(req).execute()
        return BitmapFactory.decodeStream(res.body()?.byteStream())
    }
}
